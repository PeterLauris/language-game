﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableObject : MonoBehaviour
{
    public ObjectInfo objectInfo;

    public List<Renderer> renderers;
    private List<Material> materials;
    private Color highlightColor;

    private bool isPartOfGroup;
    private WordGroup parentGroup;

    public bool lookForAnimatorInParent = false;

    // Use this for initialization
    void Start ()
    {
        if (objectInfo == null)
            objectInfo = gameObject.GetComponent<ObjectInfo>();
        if (objectInfo == null)
            Debug.LogError("WordInfo script for " + name + " not set!", this);

        materials = new List<Material>();
        if (renderers == null || renderers.Count == 0)
        {
            //TODO ko tad, ja renerer == null
            Renderer r = GetComponent<Renderer>();
            if (r != null)
            {
                foreach (Material mat in r.materials)
                    materials.Add(mat);
            }
        }
        else
        {
            for (int i = 0; i < renderers.Count; i++)
            {
                foreach (Material mat in renderers[i].materials)
                    materials.Add(mat);
            }
        }
        highlightColor = new Color(0.1f, 0.1f, 0.1f);

        parentGroup = gameObject.transform.parent.GetComponent<WordGroup>();
        if (ReferenceEquals(parentGroup, null))
        {
            parentGroup = gameObject.transform.parent.parent.GetComponent<WordGroup>();
        }
        isPartOfGroup = (parentGroup != null);
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void LeftClick() //Left Click
    {
        if (GameControl.script.gamePaused) return;
        if (!isPartOfGroup || (isPartOfGroup && ClickingControl.script.isCtrlPressed))
        {
            if (objectInfo.wordInfo != null)
            {
                //float dist = Vector3.Distance(GameControl.script.player.transform.position, transform.position);
                Debug.Log("Clicked on " + objectInfo.wordInfo.wordInTL + " - " + objectInfo.wordInfo.wordInOL);
                objectInfo.Select();
            }
            else
            {
                Debug.LogWarning("WordInfo was null for " + objectInfo.name);
            }
        }
        else
        {
            parentGroup.SelectGroup();
        }
    }

    public void RightClick()
    {
        if (GameControl.script.gamePaused) return;

        Animator a = GetAnimator();
        if (a == null) return;

        Debug.Log("Pressed right click on " + name);
        a.SetBool("Open", !a.GetBool("Open"));
    }

    public Animator GetAnimator()
    {
        Animator a;
        if (lookForAnimatorInParent)    a = gameObject.transform.parent.gameObject.GetComponent<Animator>();
        else                            a = gameObject.GetComponent<Animator>();
        return a;
    }

    public void CallHighlight()
    {
        if (GameControl.script.gamePaused) return;

        if (isPartOfGroup && (!ClickingControl.script.isCtrlPressed || parentGroup.onlyTogether)) parentGroup.HighlightGroup();
        else HighlightObject();
    }

    public void CallCancelHighlight()
    {
        if (GameControl.script.gamePaused) return;

        if (isPartOfGroup) parentGroup.CancelHighlightGroup();
        else CancelHighlightObject();
    }

    public void HighlightObject()
    {
        //Debug.Log("Highlight " + name);
        if (materials != null && materials.Count != 0)
        {
            for (int i = 0; i < materials.Count; i++)
            {
                materials[i].EnableKeyword("_EMISSION");
                materials[i].SetColor("_EmissionColor", highlightColor);
            }
        }
        else
        {
            Debug.LogWarning("Material is null or empty");
        }
    }

    public void CancelHighlightObject()
    {
        //Debug.Log("Remove Highlight " + name);
        if (materials != null && materials.Count != 0)
        {
            for (int i = 0; i < materials.Count; i++)
            {
                materials[i].EnableKeyword("_EMISSION");
                materials[i].SetColor("_EmissionColor", Color.black);
            }
        }
    }
}
