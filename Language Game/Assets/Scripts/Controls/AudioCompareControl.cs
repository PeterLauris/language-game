﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCompareControl : MonoBehaviour
{
    public AudioClip b1, b2, b3, b4, b5;

	// Use this for initialization
	void Start () {
        CompareSounds(b1, b2);
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void CompareSounds(AudioClip a1, AudioClip a2)
    {
        float[] samples1 = new float[a1.samples * a1.channels];
        a1.GetData(samples1, 0);
        float[] samples2 = new float[a2.samples * a2.channels];
        a2.GetData(samples2, 0);
    }
}
