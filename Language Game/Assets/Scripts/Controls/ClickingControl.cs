﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickingControl : MonoBehaviour
{
    public static ClickingControl script;

    public bool isCtrlPressed = false;
    public ClickableObject lastObjectEntered;

    public int ignoreColliderLayer;

    void Awake ()
    {
        script = this;

        ignoreColliderLayer = ~(1 << LayerMask.NameToLayer("Ignore Collider Layer"));
    }
	
	void Update ()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000, ignoreColliderLayer))
        {
            //Debug.Log(hit.collider.name);
            Debug.DrawLine(Camera.main.transform.position, hit.point, Color.red);
            ClickableObject co;

            if (hit.collider.name == "_collider") {
                co = hit.transform.parent.gameObject.GetComponent<ClickableObject>();
            }
            else
            {
                co = hit.collider.GetComponent<ClickableObject>();
            }

            if (co != lastObjectEntered)
            {
                if (lastObjectEntered != null) lastObjectEntered.CallCancelHighlight();
                lastObjectEntered = co;
                if (co != null)
                {
                    co.CallHighlight();

                    Animator a = co.GetAnimator();
                    if (a != null)
                    {
                        OverlayControl.script.animationAvailableObject.SetActive(true);
                    }
                    else
                    {
                        OverlayControl.script.animationAvailableObject.SetActive(false);
                    }
                }
                else
                {
                    OverlayControl.script.animationAvailableObject.SetActive(false);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            isCtrlPressed = true;
            if (lastObjectEntered != null)
            {
                lastObjectEntered.CallCancelHighlight();
                lastObjectEntered.CallHighlight();
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            isCtrlPressed = false;
            if (lastObjectEntered != null)
            {
                lastObjectEntered.CallCancelHighlight();
                lastObjectEntered.CallHighlight();
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (lastObjectEntered != null)
                lastObjectEntered.LeftClick();
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (lastObjectEntered != null)
                lastObjectEntered.RightClick();
        }
    }
}
