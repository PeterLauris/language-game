﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //StartCoroutine(SendError("123"));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator SendError(string message)
    {
        Debug.Log("Call");
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("message", message);
        //wwwForm.AddField("ValueB", "44");
        WWW www = new WWW("https://www.vocabularylife.com/game/test.php", wwwForm);
        yield return www;

        //if (!String.IsNullOrEmpty(www.error))
        //{
        //    throw new System.ApplicationException("WWW error: " + www.error);
        //}
        Debug.Log("Text from PHP: " + www.text);
        //string[] values = www.text.Split('\n');
        //foreach (string value in values)
        //{
        //    if (String.IsNullOrEmpty(value.Trim())
        //    {
        //        continue;
        //    }
        //    string variable = value.Split('=');
        //    Debug.Log("PHP data: " + variable[0].Trim() + " = " + variable[1].Trim());
        //}
    }
}
