﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class GameData
{
	public string ORIGINAL_LANGUAGE;
	public string TARGET_LANGUAGE;
    
	public int saveID;

	public DateTime lastLoginDate;

	public GameData()
	{
		ORIGINAL_LANGUAGE = "en";
		TARGET_LANGUAGE = "ru";
		saveID = 1;
		lastLoginDate = DateTime.Now;
	}
}

public class GameControl : MonoBehaviour
{
	public static bool DEBUG_MODE = true;
    public static string VERSION = "0.2.1";
    private Vector3 SPAWN_POSITION = new Vector3(0, 1.5f, 0.8f);
    private float RESPAWN_LIMIT = -1;

    public static GameControl script;
    public GameData data;

    public bool gameLoaded = false;

    public GameObject currentLocation;
    public GameObject player;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController playerController;
    public AudioSource wordSource;

    public TextMeshProUGUI dailyTipText;

	public string activeSaveSlot = "";

	public bool gamePaused = false;
	public bool shouldSaveGame = false;

    public GameObject prefab_dragIcon;

    void Awake()
    {
        script = this;
        data = new GameData();
    }

    public void Init()
    {
        playerController = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();

        activeSaveSlot = PlayerPrefs.GetString("activeSaveSlot");
        //Debug.Log("Active Slot: " + activeSaveSlot);
    }

    public void InitGame()
    {
        Init();
        WordControl.script.Init();
        ReviewControl.script.Init();
        VocabListControl.script.Init();

        SetDailyTip();
    }

	void OnApplicationPause(bool isPaused)
	{
		if (!isPaused)
		{
			CheckDaysInRow ();
		}
	}

    // Use this for initialization
    void Start ()
    {
        InitGame();

        if (SaveLoad.Load())
        {
            PopupControl.script.ShowNotification("Game loaded");
        }
        else
        {
            PopupControl.script.ShowNotification("Game failed to load");
			//TODO apskatás, vai save fails eksisté un nobackupo
		}
		CheckDaysInRow ();

		shouldSaveGame = true;
		InvokeRepeating ("UpdateSaveGame", 0.1f, 0.1f);
		InvokeRepeating ("AutoSaveGame", 60f, 60f);
        InvokeRepeating ("CheckForPlayerRespawn", 0.78f, 0.78f);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.M))
        {
            SaveLoad.Save();
        }
    }

	void UpdateSaveGame() {
        if (shouldSaveGame)
        {
            Debug.Log("Call save");
            SaveLoad.Save();
            //OverlayControl.script.progressSavedAnimator.Play("ShowImage");
            shouldSaveGame = false;
        }
	}

	public void AutoSaveGame() {
		shouldSaveGame = true;
	}

    public void CheckForPlayerRespawn()
    {
        if (player.transform.position.y < RESPAWN_LIMIT)
            player.transform.position = SPAWN_POSITION;
    }


    public void CheckDaysInRow()
	{
		if (data.lastLoginDate.AddDays(1).Date == DateTime.Now.Date) {
			StatsControl.script.data.daysInRow++;
			data.lastLoginDate = DateTime.Now;
		}
		else if (data.lastLoginDate.AddDays(1).Date < DateTime.Now.Date) {
            StatsControl.script.data.daysInRow = 1;
		}
	}

    public void SetDailyTip()
    {
        int idx = Utilities.RandomIntFromRange(0, DailyTips.DAILY_TIPS[GameControl.script.data.ORIGINAL_LANGUAGE].Count - 1, DateTime.Now.DayOfYear);
        dailyTipText.text = DailyTips.DAILY_TIPS[GameControl.script.data.ORIGINAL_LANGUAGE][idx];
    }

    public void PauseGame() {
        Debug.Log("PAUSED");
        gamePaused = true;
        playerController.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        //escPanel.SetActive(true);
        //Time.timeScale = 0;
    }

    public void UnpauseGame() {
        Debug.Log("UNPAUSED");
        gamePaused = false;
        playerController.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PanelControl.script.HideAllPanels();
        //escPanel.SetActive(false);
        //Time.timeScale = 1;
    }
}
