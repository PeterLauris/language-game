﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OverlayControl : MonoBehaviour
{
    public static OverlayControl script;

    public TextMeshProUGUI wordProgressText;

    public GameObject reviewNotification;
    private TextMeshProUGUI reviewCountText;

    public Animator progressSavedAnimator;

    public GameObject animationAvailableObject;

    private void Awake()
    {
        script = this;
    }

    // Use this for initialization
    void Start () {
        reviewCountText = reviewNotification.GetComponentInChildren<TextMeshProUGUI>();

        InvokeRepeating("UpdateTextFields", 0.08f, 0.08f);
        InvokeRepeating("UpdateReviewNotification", 5f, 60f);
	}
	
	// Update is called once per frame
	void UpdateTextFields ()
    {
        wordProgressText.text = WordControl.script.data.foundWords.Count.ToString() + "/" + WordControl.script.locationWords.Count;
    }

    public void UpdateReviewNotification()
    {
        int reviewCount = ReviewControl.script.GetReviewableWordCount();
        if (reviewCount > 0)
        {
            reviewNotification.SetActive(true);
            reviewCountText.text = reviewCount.ToString();
        }
        else
        {
            reviewNotification.SetActive(false);
        }
    }
}
