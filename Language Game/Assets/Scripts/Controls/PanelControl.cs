﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelControl : MonoBehaviour
{
    public static PanelControl script; 

    //public GameObject escPanel;
    public Panel escPanel;
	public Panel vocabPanel;
	public Panel flashcardPanel;

    //public GameObject wordPanel;

	public Queue<Panel> panelQueue;
	public Panel openedPanel;

    void Awake () {
        script = this;

		panelQueue = new Queue<Panel> ();
		openedPanel = null;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            if (GameControl.script.gamePaused)
            {
                GameControl.script.UnpauseGame();
				CloseOpenedPanel ();
            }
            else
            {
                GameControl.script.PauseGame();
				AddPanelToQueue (escPanel);
            }
        }
    }

	public void AddPanelToQueue(Panel panel) {
		panelQueue.Enqueue (panel);
		if (panelQueue.Count == 1) {
			ShowNextPanel ();
		}
	}

	public void ShowNextPanel() {
		if (panelQueue.Count == 0)
			return;

		Panel currentPanel = panelQueue.Dequeue ();
		openedPanel = currentPanel;
		openedPanel.gameObject.SetActive (true);
	}

    public void CallStartWordSearch()
    {
        HideSelectionPanel();
        WordSearchControl.script.StartWordSearch();
        GameControl.script.UnpauseGame();
    }

    public void ShowSelectionPanel()
    {
		CloseOpenedPanel ();
		AddPanelToQueue (escPanel);
    }
    public void HideSelectionPanel()
	{
		CloseOpenedPanel ();
    }

    public void ShowVocabPanel()
    {
        HideSelectionPanel();
        VocabListControl.script.OnPanelOpened(); //TODO parbauda to pie ShowNewPanel
		AddPanelToQueue (vocabPanel);
    }
    public void HideVocabPanel()
	{
		ShowSelectionPanel ();
    }

    public void ShowReviewPanel()
    {
        HideSelectionPanel();
		ReviewControl.script.OnPanelOpened(); //TODO parbauda to pie ShowNewPanel
		AddPanelToQueue(flashcardPanel);
    }
    public void HideReviewPanel()
	{
		ShowSelectionPanel ();
    }

    public void ShowWordPanel(WordInfo wordInfo)
    {
        GameControl.script.PauseGame();
        WordSelectedPanel.script.ShowPanel(wordInfo);
    }

    public void HideWordPanel()
    {
        WordSelectedPanel.script.HidePanel();
    }

    public void HideAllPanels()
    {
        HideReviewPanel();
        HideVocabPanel();
        HideSelectionPanel();
        HideWordPanel();
    }

	public void CloseOpenedPanel() {
		if (openedPanel != null) {
			openedPanel.Close ();
		}
	}
}
