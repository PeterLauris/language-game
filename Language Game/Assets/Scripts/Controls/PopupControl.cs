﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupControl : MonoBehaviour
{
    public enum PopupType
    {
        INFO
    }

    public static PopupControl script;

    public Animator notificationAnimator;

    public GameObject displayedWordPanel;
    public GameObject displayedWordNewWordPanel;
    private TextMeshProUGUI displayedWordText;

    private Queue<string> notificationsQueue;
    public bool isNotificationShowing = false;

    private Queue<object[]> popupQueue;
    public Popup currentOpenPopup;
    public InfoPopup infoPopup;


    void Awake()
    {
        script = this;
        notificationsQueue = new Queue<string>();
        popupQueue = new Queue<object[]>();
    }

    // Use this for initialization
    void Start () {
        displayedWordText = displayedWordPanel.GetComponentInChildren<TextMeshProUGUI>();
    }
	
	public void AddPopupToQueue(params object[] args)
    {
        popupQueue.Enqueue(args);
        if (popupQueue.Count == 1)
        {
            ShowNextPopup();
        }
    }

    public void ShowNextPopup()
    {
        if (popupQueue.Count == 0 || currentOpenPopup != null) return;

        object[] args = popupQueue.Dequeue();
        switch((PopupType)args[0])
        {
            case PopupType.INFO:
                ShowInfoPopup((string)args[1], (string)args[2], (bool)args[3]);
                break;
        }
        GameControl.script.PauseGame();
    }

    public void ShowInfoPopup(string title, string content, bool unpause)
    {
        infoPopup.Set(title, content, unpause);
        infoPopup.gameObject.SetActive(true);
        currentOpenPopup = infoPopup;
    }

    public void ShowNotification(string text)
    {
        notificationsQueue.Enqueue(text);
        ShowNextNotification();
    }

    public void ShowNextNotification()
    {
        if (notificationsQueue.Count == 0 || isNotificationShowing) return;

        string notif = notificationsQueue.Dequeue();
        notificationAnimator.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = notif;
        notificationAnimator.Play("Start");
        isNotificationShowing = true;
    }

    /// <summary>
    /// Attēlo izvēlēto vārdu uz ekrāna
    /// </summary>
    /// <param name="word"></param>
    public void ShowSelectedWord(string word, bool foundFirstTime = false)
    {
        displayedWordPanel.SetActive(true);
        displayedWordText.text = word;
        if (foundFirstTime)
        {
            displayedWordNewWordPanel.SetActive(true);
        }
        else
        {
            displayedWordNewWordPanel.SetActive(false);
        }

        CancelInvoke("HideSelectedWord");
        Invoke("HideSelectedWord", 3f);
    }

    public void HideSelectedWord()
    {
        displayedWordPanel.SetActive(false);
        displayedWordNewWordPanel.SetActive(false);
    }
}
