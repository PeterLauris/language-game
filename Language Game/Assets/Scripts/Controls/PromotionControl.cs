﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromotionControl : MonoBehaviour
{
    public static PromotionControl script;

    private void Awake()
    {
        script = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenTwitterLink()
    {
        Application.OpenURL("https://twitter.com/VocabularyLife");
    }

    public void OpenFacebookLink()
    {
        Application.OpenURL("https://www.facebook.com/VocabularyLife/");
    }
}
