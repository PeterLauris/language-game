﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsData
{
    public int totalWordClicked;
    public int uniqueWordsFound;
    public int daysInRow;
    public double timeSpentInGame;
    public int missionsCompleted;

    public StatsData()
    {
        totalWordClicked = 0;
        uniqueWordsFound = 0;
        daysInRow = 1;
        timeSpentInGame = 0;
        missionsCompleted = 0;
    }
}

public class StatsControl : MonoBehaviour
{
    public static StatsControl script;
    public StatsData data;

    private double timeOfLastTimeUpdate = 0;

    void Awake()
    {
        script = this;
        data = new StatsData();
    }
	
    void Start()
    {
        InvokeRepeating("UpdateTimeSpent", 30f, 30f);
    }

    void UpdateTimeSpent()
    {
        data.timeSpentInGame += (Time.time - timeOfLastTimeUpdate);
        timeOfLastTimeUpdate = Time.time;
    }
}
