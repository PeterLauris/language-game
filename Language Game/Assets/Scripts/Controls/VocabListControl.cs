﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VocabListControl : MonoBehaviour
{
    public static VocabListControl script;

    public GameObject wordContainer;
    public List<WordLine> displayedWords;

    public Button previousPage;
    public Button nextPage;

    private int pageWordIndex = 0;
    private const int WORDS_PER_PAGE = 10;
    private int pageCount;

    private void Awake()
    {
        script = this;
    }

    // Use this for initialization
    void Start ()
    {

	}

    public void Init()
    {
        CalculatePageCount();
        RedrawWords();
        RedrawButtons();
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void OnPanelOpened()
    {
        RedrawWords();
    }

    //Attēlo 10 no meklējamajiem vārdiem
    public void RedrawWords()
    {
        for (int i = 0; i < WORDS_PER_PAGE; i++) {
            int idx = pageWordIndex * WORDS_PER_PAGE + i;
            if (idx < WordControl.script.locationWords.Count) {
                displayedWords[i].Set(WordControl.script.locationWords[idx]);
                displayedWords[i].gameObject.SetActive(true);
            }
            else {
                displayedWords[i].gameObject.SetActive(false);
            }
        }
    }

    public void CalculatePageCount()
    {
        pageCount = Mathf.CeilToInt(WordControl.script.locationWords.Count / WORDS_PER_PAGE);
    }

    public void RedrawButtons()
    {
        if (pageWordIndex == 0) previousPage.interactable = false;
        else previousPage.interactable = true;

        if (pageWordIndex == pageCount - 1) nextPage.interactable = false;
        else nextPage.interactable = true;
    }

    public void PreviousPage()
    {
        if (pageWordIndex == 0)
            return;
        pageWordIndex--;
        RedrawWords();
        RedrawButtons();
    }

    public void NextPage()
    {
        if (pageWordIndex >= pageCount - 1)
            return;
        pageWordIndex++;
        RedrawWords();
        RedrawButtons();
    }
}
