﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordData
{
    public List<WordInfo> foundWords;
    public List<DateTime> foundWordTimes;
}

public class WordControl : MonoBehaviour
{
    public static WordControl script;
    public WordData data;

    public Dictionary<string, string> dictionary;
    public List<string> dictionaryKeys;

    public Dictionary<string, string> OLWords; //Key - word in English, Value - OLword
    
    public List<WordInfo> locationWords;
    public Dictionary<string, WordInfo> wordInfos;
    public Dictionary<string, WordSaveInfo> wordSaveInfos;

    public List<string[]> wordChildren;

    public DateTime lastClickTime;
    public WordInfo lastClickWord;

    // Use this for initialization
    void Awake () {
        script = this;
        data = new WordData();

        data.foundWords = new List<WordInfo>();
    }

    void Start()
    {

        //DEBUG_FindAllWords();
    }

    public void Init()
    {
        dictionary = Utilities.GetDictionary("en-" + GameControl.script.data.TARGET_LANGUAGE);
        dictionaryKeys = new List<string>(dictionary.Keys);

        OLWords = Utilities.GetOLWords("later/OL-" + GameControl.script.data.ORIGINAL_LANGUAGE);

        wordChildren = Utilities.GetWordChildren();

        wordInfos = new Dictionary<string, WordInfo>();
        wordSaveInfos = new Dictionary<string, WordSaveInfo>();
        lastClickTime = DateTime.Now;

        InitLocationWords();
        InitChildWords();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void InitLocationWords()
    {
        locationWords = new List<WordInfo>();
        ObjectInfo[] allWords = GameControl.script.currentLocation.GetComponentsInChildren<ObjectInfo>();

        foreach (ObjectInfo wordObject in allWords)
        {
            //Debug.Log(wordObject.wordInEnglish);

            if (wordInfos.ContainsKey(wordObject.wordInEnglish))
            {
                wordObject.wordInfo = wordInfos[wordObject.wordInEnglish];
            }
            else
            {
                wordObject.wordInfo = NewWordInfo(wordObject.wordInEnglish);
                //Debug.Log("Added " + wordObject.wordInEnglish);
            }
        }
        try
        {
            locationWords.Sort((a, b) => a.wordInTL.CompareTo(b.wordInTL));
        }
        catch
        {
            Debug.LogWarning("Error while sorting... wordInTL is null");
        }
    }

    public void InitChildWords()
    {
        for (int i = 0; i < wordChildren.Count; i++)
        {
            if (wordInfos.ContainsKey(wordChildren[i][0]))
            {
                WordInfo nounInfo = wordInfos[wordChildren[i][0]];
                //nounInfo.saveInfo.isUnlocked = true;
                if (wordChildren[i][1] != "")
                {
                    WordInfo verbInfo;
                    if (wordInfos.ContainsKey(wordChildren[i][1]))
                    {
                        verbInfo = wordInfos[wordChildren[i][1]];
                    }
                    else
                    {
                        verbInfo = WordControl.script.NewWordInfo(wordChildren[i][1]);
                    }
                    nounInfo.childVerb = verbInfo;
                }
                if (wordChildren[i][2] != "")
                {
                    WordInfo adjectiveInfo;
                    if (wordInfos.ContainsKey(wordChildren[i][2]))
                    {
                        adjectiveInfo = wordInfos[wordChildren[i][2]];
                    }
                    else
                    {
                        adjectiveInfo = WordControl.script.NewWordInfo(wordChildren[i][2]);
                    }
                    nounInfo.childAdjective = adjectiveInfo;
                }
            }
            else
            {
                //Debug.LogWarning("Noun not found in location. Skip - " + wordChildren[i][0]);
            }
        }
    }

    public WordInfo NewWordInfo(string wordInEnglish)
    {
        bool assumeEnglish = false;
        if (!OLWords.ContainsKey(wordInEnglish))
        {
            //Debug.LogWarning("OLWord not found. Assume English");
            assumeEnglish = true;
            //return null;
        }

        WordInfo wi = new WordInfo();
        if (wordSaveInfos.ContainsKey(wordInEnglish))
        {
            wi.saveInfo = wordSaveInfos[wordInEnglish];
        }
        else
        {
            WordSaveInfo saveInfo = new WordSaveInfo(wordInEnglish);
            wordSaveInfos.Add(wordInEnglish, saveInfo);
            wi.saveInfo = saveInfo;
        }

        wi.wordInOL = (assumeEnglish ? wordInEnglish : OLWords[wordInEnglish]);
        //Debug.Log(wordInEnglish + " " + OLWords[wordInEnglish]);
        wi.SetTL();
        wordInfos.Add(wordInEnglish, wi);
        locationWords.Add(wi);
        return wi;
    }

    public void DEBUG_FindAllWords()
    {
        if (GameControl.DEBUG_MODE)
        {
            //foreach (KeyValuePair<WordInfo, bool> k in locationWords)
            for (int i = 0; i < locationWords.Count; i++)
            {
                if (!data.foundWords.Contains(locationWords[i]))
                {
                    data.foundWords.Add(locationWords[i]);
                    ReviewControl.script.AddWordToBoxes(locationWords[i]);
                }
            }
			//GameControl.script.AutoSaveGame();
        }
    }
}
