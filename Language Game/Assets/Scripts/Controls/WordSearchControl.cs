﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WordSearchControl : MonoBehaviour
{
    public static WordSearchControl script;

    private int SEARCHABLE_WORD_COUNT = 3;

    public List<WordInfo> searchableWords;
    public int currentSearchIdx;

    public bool isWordSearchActive;

    public GameObject nextWordPanel;
    private TextMeshProUGUI nextWordText;

    void Awake()
    {
        script = this;

        isWordSearchActive = false;
        searchableWords = new List<WordInfo>();
    }

	// Use this for initialization
	void Start () {
        nextWordText = nextWordPanel.GetComponentInChildren<TextMeshProUGUI>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F))
            StartWordSearch();
	}

    public void WordClicked(WordInfo wordClicked)
    {
        if (wordClicked == searchableWords[currentSearchIdx] || wordClicked.childVerb == searchableWords[currentSearchIdx] || wordClicked.childAdjective == searchableWords[currentSearchIdx])
        {
            Debug.Log("CORRECT!");
            if (currentSearchIdx == SEARCHABLE_WORD_COUNT - 1) EndWordSearch();
            else SearchNextWord();
        }
        else
        {
            Debug.Log("INCORRECT! " + wordClicked + " " + searchableWords[currentSearchIdx]);
        }
    }

    public void StartWordSearch()
    {
        if (isWordSearchActive)
        {
            PopupControl.script.ShowNotification("Search already active!");
            return;
        }
        if (WordControl.script.data.foundWords.Count < SEARCHABLE_WORD_COUNT)
        {
            PopupControl.script.ShowNotification("Not enough words found!");
            return;
        }

        searchableWords = Utilities.GetRandomList(WordControl.script.data.foundWords, SEARCHABLE_WORD_COUNT);
        currentSearchIdx = -1;
        SearchNextWord();
        isWordSearchActive = true;
        nextWordPanel.SetActive(true);
    }

    public void SearchNextWord()
    {
        currentSearchIdx++;

        nextWordText.text = searchableWords[currentSearchIdx].wordInTL;
        searchableWords[currentSearchIdx].PlaySound();
        Debug.Log("Searchable Word: " + searchableWords[currentSearchIdx]);
    }

    public void EndWordSearch()
    {
        isWordSearchActive = false;
        nextWordPanel.SetActive(false);
        Debug.Log("Word Search Ended");
    }
}
