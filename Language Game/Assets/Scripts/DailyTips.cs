﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DailyTips
{
    public static Dictionary<string, List<string>> DAILY_TIPS = new Dictionary<string, List<string>>()
    {
        {
            "en",
            new List<string>()
            {
                "Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1Tip 1",
                "Tip 2Tip 2Tip 2Tip 2Tip 2",
                "Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3Tip 3",
                "Tip 4Tip 4Tip 4Tip 4Tip 4Tip 4Tip 4Tip 4Tip 4",
                "Tip 5",
                "Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6Tip 6",
            }
        }
    };
}
