﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 0414
public class GridLayoutScaler : MonoBehaviour {

    GridLayoutGroup gridLayoutGroup;
    RectTransform rect;
    private int cellCount = 2;

    void Start()
    {
        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        rect = GetComponent<RectTransform>();

        gridLayoutGroup.cellSize = new Vector2(rect.rect.width, rect.rect.height);
        cellCount = GetComponentsInChildren<RectTransform>().Length;
    }

    void OnRectTransformDimensionsChange()
    {
        if (gridLayoutGroup != null && rect != null)
            //if ((rect.rect.height + (gridLayoutGroup.padding.horizontal * 2)) * cellCount < rect.rect.width)
                gridLayoutGroup.cellSize = new Vector2(rect.rect.width, rect.rect.height);
    }
}
