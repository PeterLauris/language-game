﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio; // required for dealing with audiomixers

[RequireComponent(typeof(AudioSource))]
public class MicrophoneListener : MonoBehaviour
{
    ////option to toggle the microphone listenter on startup or not
    //public bool startMicOnStartup = true;

    ////allows start and stop of listener at run time within the unity editor
    //public bool stopMicrophoneListener = false;
    //public bool startMicrophoneListener = false;

    //private bool microphoneListenerOn = false;

    ////public to allow temporary listening over the speakers if you want of the mic output
    ////but internally it toggles the output sound to the speakers of the audiosource depending
    ////on if the microphone listener is on or off
    //public bool disableOutputSound = false;
 
    // //an audio source also attached to the same object as this script is
    // AudioSource src;

    ////make an audio mixer from the "create" menu, then drag it into the public field on this script.
    ////double click the audio mixer and next to the "groups" section, click the "+" icon to add a 
    ////child to the master group, rename it to "microphone".  Then in the audio source, in the "output" option, 
    ////select this child of the master you have just created.
    ////go back to the audiomixer inspector window, and click the "microphone" you just created, then in the 
    ////inspector window, right click "Volume" and select "Expose Volume (of Microphone)" to script,
    ////then back in the audiomixer window, in the corner click "Exposed Parameters", click on the "MyExposedParameter"
    ////and rename it to "Volume"
    //public AudioMixer masterMixer;


    //float timeSinceRestart = 0;


    //void Start()
    //{
    //    //start the microphone listener
    //    if (startMicOnStartup)
    //    {
    //        RestartMicrophoneListener();
    //        StartMicrophoneListener();
    //    }

    //    InvokeRepeating("CompareSounds", 0.1f, 0.1f);
    //}

    //void Update()
    //{
    //    DisableSound(!disableOutputSound);
    //}

    //void CompareSounds()
    //{
    //    if (microphoneListenerOn && src != null && src.clip != null)
    //    {
    //        Debug.Log("TODO salīdzina skaņas");
    //        //src.clip.
    //    }
    //}

    ////stops everything and returns audioclip to null
    //public void StopMicrophoneListener()
    //{
    //    microphoneListenerOn = false;
    //    disableOutputSound = false;
    //    src.Stop();//remove mic from audiosource clip
    //    src.clip = null;
    //    Microphone.End(null);
    //    Debug.Log("Microphone stopped");
    //}


    //public void StartMicrophoneListener()
    //{
    //    microphoneListenerOn = true;
    //    disableOutputSound = true;
    //    RestartMicrophoneListener();

    //    MicrophoneIntoAudioSource(microphoneListenerOn);

    //    masterMixer.SetFloat("MasterVolume", 0);

    //    Debug.Log("Microphone started");
    //}



    //// restart microphone removes the clip from the audiosource
    //public void RestartMicrophoneListener()
    //{
    //    src = GetComponent<AudioSource>();
    //    src.clip = null; //remove any soundfile in the audiosource
    //    timeSinceRestart = Time.time;
    //    Debug.Log("Microphone restarted");
    //}

    ////puts the mic into the audiosource
    //void MicrophoneIntoAudioSource(bool MicrophoneListenerOn)
    //{
    //    if (MicrophoneListenerOn)
    //    {
    //        //pause a little before setting clip to avoid lag and bugginess
    //        if (!Microphone.IsRecording(null))
    //        {
    //            src.clip = Microphone.Start(null, true, 5, 44100);
    //            //src.loop = true;
    //            //src.mute = true;

    //            //wait until microphone position is found (?)
    //            while (!(Microphone.GetPosition(null) > 0)) { }

    //            src.Play(); // Play the audio source

    //            Invoke("StopMicrophoneListener", 4.9f);
    //        }
    //    }
    //}

    ////controls whether the volume is on or off, use "off" for mic input (dont want to hear your own voice input!) 
    ////and "on" for music input
    //public void DisableSound(bool SoundOn)
    //{
    //    float volume = 0;

    //    if (SoundOn)
    //    {
    //        volume = 0.0f;
    //    }
    //    else
    //    {
    //        volume = -80.0f;
    //    }

    //    masterMixer.SetFloat("MasterVolume", volume);
    //}
}