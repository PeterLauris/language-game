﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MissionControl : MonoBehaviour
{
    public static MissionControl script;

    public MissionInfo activeMission;
    private int currentStepIdx;

    public GameObject missionContainer;
    public TextMeshProUGUI missionStepText;
    public List<MissionItem> missionItems;
    public GameObject missionItemsContainer;

    public GameObject prefab_missionItem;

    void Awake()
    {
        script = this;
    }

	// Use this for initialization
	void Start () {
        InvokeRepeating("UpdateMission", 0.15f, 0.15f);

        //PopupControl.script.AddPopupToQueue(PopupControl.PopupType.INFO, "Info title", "Info content", true);
    }
	
	// Update is called once per frame
	void UpdateMission () {
		if (activeMission != null)
        {
            if (AreAllWordFound())
            {
                NextStep();
            }
        }
	}

    public void StartMission(int id)
    {
        Debug.Log("Start mission");
        if (MISSIONS.ContainsKey(id))
        {
            activeMission = MISSIONS[id];
            currentStepIdx = -1;
            NextStep();

            missionContainer.SetActive(true);
        }
    }

    public void EndMission()
    {
        Debug.Log("End mission");
        missionContainer.SetActive(false);
        activeMission = null;

        

        StatsControl.script.data.missionsCompleted++;
        GameControl.script.AutoSaveGame();
    }

    public void CheckForFoundWord(string wordInEnglish)
    {
        if (activeMission == null) return;

        if (activeMission.steps[currentStepIdx].missionWords.Contains(wordInEnglish)) {
            for (int i = 0; i < activeMission.steps[currentStepIdx].missionWords.Count; i++)
            {
                if (wordInEnglish == activeMission.steps[currentStepIdx].missionWords[i])
                {
                    //missionWords idx sakrīt ar missionItems idx
                    missionItems[i].isFound = true;
                    missionItems[i].checkmark.SetActive(true);
                }
            }
        }
    }
    
    public bool AreAllWordFound()
    {
        for (int i = 0; i < missionItems.Count; i++)
        {
            if (!missionItems[i].isFound) return false;
        }
        return true;
    }

    public void NextStep()
    {
        currentStepIdx++;
        if (currentStepIdx <= activeMission.steps.Count - 1)
        {
            Debug.Log("Next step");
            SpawnStepItems();
            missionStepText.text = "Step " + (currentStepIdx + 1) + "/" + activeMission.steps.Count;
        }
        else
        {
            EndMission();
        }
    }

    public void SpawnStepItems()
    {
        ClearStepItems();

        for (int i = 0; i < activeMission.steps[currentStepIdx].missionWords.Count; i++)
        {
            GameObject go = Instantiate(prefab_missionItem, missionItemsContainer.transform);
            MissionItem mi = go.GetComponent<MissionItem>();
            mi.wordText.text = activeMission.steps[currentStepIdx].missionWords[i];
            mi.checkmark.SetActive(false);
            missionItems.Add(mi);
        }
    }

    public void ClearStepItems()
    {
        for (int i = missionItems.Count - 1; i >= 0; i--)
        {
            Destroy(missionItems[i].gameObject);
        }
        missionItems.Clear();
    }

    public bool IsMissionAvailable(int missionID)
    {
        List<string> requiredWords = GetMissionRequiredWords(missionID);
        for (int i = 0; i < requiredWords.Count; i++)
        {
            if (!WordControl.script.wordInfos[requiredWords[i]].saveInfo.isFound) return false;
        }
        return true;
    }

    public List<string> GetMissionRequiredWords(int missionID)
    {
        List<string> res = new List<string>();
        MissionInfo mi = MISSIONS[missionID];
        if (mi == null) return null;

        for (int i = 0; i < mi.steps.Count; i++)
        {
            for (int j = 0; j < mi.steps[i].missionWords.Count; j++)
            {
                if (!res.Contains(mi.steps[i].missionWords[j]))
                {
                    res.Add(mi.steps[i].missionWords[j]);
                }
            }
        }
        return res;
    }

    public Dictionary<int, MissionInfo> MISSIONS = new Dictionary<int, MissionInfo>()
    {
        {
            1, //vienāds ar misijas ID
            new MissionInfo()
            {
                ID = 1,
                missionName = "Test mission 1",
                steps = new List<MissionStep>() {
                    new MissionStep()
                    {
                        descriptionBefore = "Find a table",
                        descriptionAfter = "Nice job!",
                        missionWords = new List<string>() { "table" },
                    },
                    new MissionStep()
                    {
                        descriptionBefore = "Find a book",
                        missionWords = new List<string>() { "book", "lamp" },
                    },
                    new MissionStep()
                    {
                        descriptionBefore = "Find a table again",
                        descriptionAfter = "Woohoo",
                        missionWords = new List<string>() { "table" },
                    }
                }
            }
        }
    };
}
