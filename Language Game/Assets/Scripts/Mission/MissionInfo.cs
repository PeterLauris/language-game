﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionInfo
{
    public int ID;
    public string missionName;
    public List<MissionStep> steps;
}
