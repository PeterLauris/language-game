﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MissionItem : MonoBehaviour
{
    public bool isFound = false;

    public GameObject checkmark;
    public TextMeshProUGUI wordText;
}