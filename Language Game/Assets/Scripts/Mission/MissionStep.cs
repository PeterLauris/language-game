﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionStep
{
    public string descriptionBefore;
    public string descriptionAfter;
    public List<string> missionWords;
}
