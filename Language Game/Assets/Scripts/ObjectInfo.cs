﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ClickableObject))]
//[RequireComponent(typeof(RightButtonEvent))]
public class ObjectInfo : MonoBehaviour
{
    public string wordInEnglish;
    public WordInfo wordInfo;


    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Select() {
        bool foundFirstTime = false;
        if (!wordInfo.saveInfo.isFound) //atrod pirmo reizi
        {
            wordInfo.FindWord();
            wordInfo.saveInfo.isFound = true;
            foundFirstTime = true;
            ReviewControl.script.data.newWordsBeforeReview++;
            StatsControl.script.data.uniqueWordsFound++;
        }
        StatsControl.script.data.totalWordClicked++;

        if (WordControl.script.lastClickWord == wordInfo && (DateTime.Now - WordControl.script.lastClickTime).TotalMilliseconds < 350)
        {
            PanelControl.script.ShowWordPanel(wordInfo);
        }
        else
        {
            wordInfo.PlaySound();
            if (WordSearchControl.script.isWordSearchActive)
            {
                WordSearchControl.script.WordClicked(wordInfo);
            }
            MissionControl.script.CheckForFoundWord(wordInEnglish);

            PopupControl.script.ShowSelectedWord(wordInfo.wordInTL, foundFirstTime);
        }

        WordControl.script.lastClickTime = DateTime.Now;
        WordControl.script.lastClickWord = wordInfo;

        if (foundFirstTime)
        {
            GameControl.script.AutoSaveGame();
        }
    }
}
