﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour {

	public void Close() {
		gameObject.SetActive (false);
		PanelControl.script.ShowNextPanel ();
	}
}
