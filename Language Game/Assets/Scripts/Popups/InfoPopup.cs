﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoPopup : Popup
{
    public bool unpauseGame;

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI contentText;

    public void Set(string title, string content, bool unpause)
    {
        titleText.text = title;
        contentText.text = content;
        unpauseGame = unpause;
    }

    public override void Close(bool _u = false)
    {
        base.Close(unpauseGame);
    }
}
