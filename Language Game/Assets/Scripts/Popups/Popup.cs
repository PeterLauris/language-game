﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : MonoBehaviour
{
    public virtual void Close(bool unpause = false)
    {
        this.gameObject.SetActive(false);
        if (unpause)
        {
            GameControl.script.UnpauseGame();
        }
    }
}
