﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDropControl : MonoBehaviour
{
    public static DragDropControl script;

    public const int DRAG_DROP_WORD_COUNT = 7;

    public List<DragDropRow> dragDropRows;

    public int[] currentIdxOrder;
    public List<int> correctIdxOrder;

    private void Awake()
    {
        script = this;
    }

    public void StartDragDrop()
    {
        if (WordControl.script.data.foundWords.Count < DRAG_DROP_WORD_COUNT)
        {
            return;
        }

        List<WordInfo> selectedWords = new List<WordInfo>();

        for (int i = 0; i < DRAG_DROP_WORD_COUNT; i++)
        {
            selectedWords.Add(WordControl.script.data.foundWords[i]);
        }
        
        for (int i = 0; i < DRAG_DROP_WORD_COUNT; i++)
        {
            dragDropRows[i].dragItem.itemText.text = selectedWords[i].wordInTL;
            dragDropRows[i].rightText.text = selectedWords[i].wordInOL;
        }
    }
}
