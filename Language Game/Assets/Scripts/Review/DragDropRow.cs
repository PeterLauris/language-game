﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DragDropRow : MonoBehaviour
{
    public DragAndDropCell dragCell;
    public DragAndDropCell dropCell;
    public DragAndDropItem dragItem;
    public TextMeshProUGUI rightText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
