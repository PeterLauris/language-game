﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class ReviewData
{
    public List<List<WordInfo>> boxes;
    public int newWordsBeforeReview;
}

public class ReviewControl : MonoBehaviour
{
    public static ReviewControl script;
    public ReviewData data;

    public FlashcardInfo flashcardInfo;
    public Animator flashcardAnimator;

    public GameObject flashcardContainer;
    public GameObject feedbackContainer;
    public Button startButton;

    public GameObject noWordsGO;

    private const int BOXES_COUNT = 10;
    private int[] BOX_SELECT_MINUTES = new int[BOXES_COUNT] { 1, 5, 15, 30, 60, 120, 240, 720, 2160, 4320 };

    public WordInfo currentWord;
    private List<WordInfo> unusedWords; //words used for selecting flashcards
    private List<WordInfo> selectedWords;

	// Use this for initialization
	void Awake () {
        script = this;
        data = new ReviewData();

        data.boxes = new List<List<WordInfo>>();
        for (int i = 0; i < BOXES_COUNT; i++)
        {
            data.boxes.Add(new List<WordInfo>());
        }
	}

    public void Init()
    {

    }

    public void OnPanelOpened()
    {
        EndFlashcardsSession();
        StartFlashcardsSession();
    }
	
    //Tiek izsaukta, kad panelis tiek atvērts
	public void StartFlashcardsSession()
    {
        PrepareWordList();
        if (selectedWords.Count == 0)
        {
            //PopupControl.script.ShowNotification("No words need to be reviewed!");
            noWordsGO.SetActive(true);
            return;
        }
        else
        {
            noWordsGO.SetActive(false);
        }
        TryNextFlashcard();
        //startButton.gameObject.SetActive(false);
        flashcardContainer.SetActive(true);
        feedbackContainer.SetActive(false);
    }

    public void EndFlashcardsSession()
    {
        //startButton.gameObject.SetActive(true);
        flashcardContainer.SetActive(false);
        feedbackContainer.SetActive(false);
    }

    public void TryNextFlashcard()
    {
        if (!ShowNextWord())
        {
            Debug.LogWarning("No more words selected");
            noWordsGO.SetActive(true);
            EndFlashcardsSession();
            OverlayControl.script.UpdateReviewNotification();
            GameControl.script.AutoSaveGame();
        }
    }

    public void ShowFlashcard(WordInfo word)
    {
        currentWord = word;
        flashcardInfo.sourceWordText.text = word.wordInOL;
        flashcardInfo.targetWordText.text = word.wordInTL;

        flashcardAnimator.Play("Nothing");
        feedbackContainer.SetActive(false);
    }

    public void TurnFlashcard()
    {
        flashcardAnimator.Play("FlashcardFlip");
        feedbackContainer.SetActive(true);
    }

    public void AddWordToBoxes(WordInfo word)
    {
        if (DoBoxesContainWord(word)) return;

        data.boxes[0].Add(word);
    }

    public void PrepareWordList()
    {
        selectedWords = new List<WordInfo>();
        for (int i = 0; i < BOXES_COUNT; i++)
        {
            data.boxes[i].Shuffle();
            for (int j = 0; j < data.boxes[i].Count; j++)
            {
                if (ShouldReviewWord(data.boxes[i][j]))
                {
                    AddWordFromLevel(i);
                }
                //if (!AddWordFromLevel(i)) break;
            }
        }
        Debug.Log("Selected words count: " + selectedWords.Count);
    }

    public int GetReviewableWordCount()
    {
        int res = 0;
        for (int i = 0; i < BOXES_COUNT; i++)
        {
            for (int j = 0; j < data.boxes[i].Count; j++)
            {
                if (ShouldReviewWord(data.boxes[i][j])) res++;
            }
        }
        return res;
    }

    private bool AddWordFromLevel (int level)
    {
        if (level < 0) return false;
        
        for (int i = 0; i < data.boxes[level].Count; i++)
        {
            if (selectedWords.Contains(data.boxes[level][i])) continue; //vārds jau ir izvēlēts
            selectedWords.Add(data.boxes[level][i]);
            return true;
        }

        //ja tiek ciklam cauri, tad līmenī visi vārdi ir izmantoti
        //skatās iepriekšējo līmeni
        return AddWordFromLevel(level-1);
    }

    private int GetWordBox(WordInfo word)
    {
        for (int i = 0; i < BOXES_COUNT; i++)
        {
            for (int j = 0; j < data.boxes[i].Count; j++)
            {
                if (data.boxes[i][j] == word) return i;
            }
        }

        return -1;
    }

    //Parāda sākamo vārdu no izvēlēto vārdu saraksta
    public bool ShowNextWord()
    {
        if (selectedWords.Count == 0) return false;

        ShowFlashcard(selectedWords[0]);
        selectedWords.RemoveAt(0);

        return true;
    }
    
    public void GiveFeedback(int response)
    {
        //1 - labi, 2 - vidēji, 3 - slikti
        int wordBoxNumber = GetWordBox(currentWord);
        Debug.Log(wordBoxNumber);
        if (wordBoxNumber == -1) return;

        switch (response)
        {
            case 1:
                //kaste tiek palielināta par 1
                if (wordBoxNumber < BOXES_COUNT - 1)
                {
                    data.boxes[wordBoxNumber].Remove(currentWord);
                    data.boxes[wordBoxNumber+1].Add(currentWord);
                }
                break;
            case 2:
                //kaste tiek samazināta par 1
                if (wordBoxNumber > 0)
                {
                    data.boxes[wordBoxNumber].Remove(currentWord);
                    data.boxes[wordBoxNumber - 1].Add(currentWord);
                }
                break;
            case 3:
                //atliek atpakaļ uz sākumu
                data.boxes[wordBoxNumber].Remove(currentWord);
                data.boxes[0].Add(currentWord);
                break;
        }

        currentWord.saveInfo.reviewInfo.lastReviewTime = DateTime.Now;

        TryNextFlashcard();
    }

    public bool DoBoxesContainWord(WordInfo word)
    {
        return GetWordBox(word) != -1;
    }

    private bool ShouldReviewWord(WordInfo word)
    {
        TimeSpan timeSinceLastReview = (DateTime.Now - word.saveInfo.reviewInfo.lastReviewTime);
        //Debug.Log("Time since last review for " + word.wordInOL + " = " + timeSinceLastReview.TotalMinutes);
        return (timeSinceLastReview.TotalMinutes > BOX_SELECT_MINUTES[GetWordBox(word)]);
    }
     
    public void ResetReviewProgress()
    {
        for (int i = 1; i < data.boxes.Count; i++)
        {
            for (int j = data.boxes[i].Count - 1; j >= 0; j--)
            {
                data.boxes[0].Add(data.boxes[i][j]);
            }
            data.boxes[i].Clear();
        }

        for (int j = 0; j < data.boxes.Count; j++)
        {
            data.boxes[0][j].saveInfo.reviewInfo.lastReviewTime = DateTime.Now.AddMonths(-1);
        }
    }

    public void PrintBoxes()
    {
        for (int i = 0; i < BOXES_COUNT; i++)
        {
            Debug.Log("Box " + i);
            string s = "";
            for (int j = 0; j < data.boxes[i].Count; j++)
            {
                s += data.boxes[i][j] + " ";
            }
            Debug.Log(s);
        }
    }
}
