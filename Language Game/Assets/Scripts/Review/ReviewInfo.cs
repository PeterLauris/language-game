﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ReviewInfo
{
    public DateTime lastReviewTime;
    public bool firstReviewDone;

    public ReviewInfo()
    {
        lastReviewTime = DateTime.Now.AddMonths(-1);
        firstReviewDone = false;
    }
}
