﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;

[ExecuteInEditMode]
[AddComponentMenu("Event/RightButtonEvent")]
[RequireComponent(typeof(Animator))]
public class RightButtonEvent : MonoBehaviour
{
    [System.Serializable] public class RightButton : UnityEvent { }
    private RightButton onRightDown;
    private RightButton onRightUp;
    private bool isOver = false;
    void Start()
    {
    }

    void Update()
    {
        if (isOver && Input.GetMouseButtonDown(1))
        {
            //onRightDown.Invoke();
            //gameObject.GetComponent<ClickableObject>().RightClick();
        }
        //if (Input.GetMouseButtonUp(1))
        //{
        //    onRightUp.Invoke();
        //}
    }

    public void OnMouseEnter()
    {
        isOver = true;
        //Debug.Log("isOver true for " + name);
    }

    public void OnMouseExit()
    {
        isOver = false;
        //Debug.Log("isOver false for " + name);
    }
}