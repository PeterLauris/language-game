﻿using UnityEngine;    // For Debug.Log, etc.

using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections.Generic;

// === This is the info container class ===
[Serializable()]
public class SaveData : ISerializable
{
    public string version;

    // === Values ===
    // Edit these during gameplay
    public DateTime time;
    public string str;

    // === /Values ===

    // The default constructor. Included for when we call it during Save() and Load()
    public SaveData() { version = GameControl.VERSION; }
    public SaveData(string _version) { version = _version; }

    public static string GetSaveString()
    {
        string res = "";

        //WordSaveInfos
        res += "wsi-";
        int idx = 0;
        foreach (KeyValuePair<string, WordSaveInfo> w in WordControl.script.wordSaveInfos)
        {
            if (idx > 0) res += "|";

            WordSaveInfo wsi = w.Value;
            res += wsi.wordInEnglish + ";" + wsi.foundTime + ";" + wsi.synonymIdx + ";" + wsi.reviewInfo.lastReviewTime;

            idx++;
        }

        //---GameControl
        res += "=origLang-" + GameControl.script.data.ORIGINAL_LANGUAGE;
        res += "=targetLang-" + GameControl.script.data.TARGET_LANGUAGE;
        res += "=sid-" + GameControl.script.data.saveID;
        res += "=lastLogin-" + GameControl.script.data.lastLoginDate;
        res += "=inRow-" + StatsControl.script.data.daysInRow;

        //---ReviewControl
        res += "=boxes-";
        for (int i = 0; i < ReviewControl.script.data.boxes.Count; i++)
        {
            if (i > 0) res += "|";
            for (int j = 0; j < ReviewControl.script.data.boxes[i].Count; j++)
            {
                if (j > 0) res += ";";
                res += ReviewControl.script.data.boxes[i][j].saveInfo.wordInEnglish;
            }
        }
        res += "=wordsBeforeReview-" + ReviewControl.script.data.newWordsBeforeReview;

        //---WordControl
        res += "=foundWords-";
        for (int i = 0; i < WordControl.script.data.foundWords.Count; i++)
        {
            if (i > 0) res += "|";
            res += WordControl.script.data.foundWords[i].saveInfo.wordInEnglish;
        }


        return res;
    }
    public static void SetSaveString(string str)
    {
        string[] parameters = str.Split('=');
        foreach (string parameter in parameters)
        {
            try
            {
                string[] split = parameter.Split('-');

                //Debug.Log("Set " + split[0]);
                switch (split[0])
                {
                    case "mon":
                        //GameControl.script.data.Money = new BigNumber(split[1]);
                        break;
                    
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning("Exception while loading - " + e.Message + " " + e.StackTrace);
            }
        }
    }

    // This constructor is called automatically by the parent class, ISerializable
    // We get to custom-implement the serialization process here
    public SaveData(SerializationInfo info, StreamingContext ctxt)
    {
        // Get the values from info and assign them to the appropriate properties. Make sure to cast each variable.
        // Do this for each var defined in the Values section above

        //saveGameVersion = (string)info.GetValue("saveGameVersion", typeof(string));
        time = (DateTime)info.GetValue("time", typeof(DateTime));
        str = (string)info.GetValue("str", typeof(string));
        //gameControlData = (GameControlData)info.GetValue("gameControlData", typeof(GameControlData));
        //moneyControlData = (MoneyControlData)info.GetValue("moneyControlData", typeof(MoneyControlData));
    }

    // OLD
    // Required by the ISerializable class to be properly serialized. This is called automatically
    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        // Repeat this for each var defined in the Values section
        //gameControlData = GameControl.script.data;
        //moneyControlData = MoneyControl.script.data;

        PrepareInfo(info);
    }

    public void GetObjectData()
    {
        // Repeat this for each var defined in the Values section
        time = DateTime.Now;
        str = GetSaveString();
    }

    public void PrepareInfo(SerializationInfo info)
    {
        info.AddValue("time", DateTime.Now);
        info.AddValue("str", GetSaveString());
    }
}

// === This is the class that will be accessed from scripts ===
public class SaveLoad
{
    static string HASH_DELIMITER = "||";
    public static string hash;

    // Call this to write data
    public static void Save(bool encrypt = true)  // Overloaded
    {
        if (!GameControl.script.gameLoaded) return; //if the prefiouse game is not loaded, don't overwrite it
        
        Save(SaveFilePath(), encrypt);
    }
    public static void Save(string filePath, bool encrypt)
    {
        string serString = GetSaveString();
        File.WriteAllText(filePath, serString);
    }

    public static string GetSaveString()
    {
        SaveData data = new SaveData();
        data.GetObjectData();
        GameControl.script.data.saveID++;

        string serString = StringSerializationAPI.Serialize(typeof(SaveData), data);
        hash = Utilities.CalculateMD5Hash(serString);
        serString = hash + HASH_DELIMITER + serString;

        return Utilities.Encrypt(serString);
    }

    public static bool Load(bool gameStart = true)
    {
        try
        {
            SaveData data = VerifySaveData(SaveFilePath());
            string saveString = data.str;

            if (data == null || data.str == null)
            {
                Debug.LogWarning("data or str is null!!");
                throw new System.Exception("data was not loaded");
            }

            //TimeSpan timeTaken = DateTime.Now - data.time;
            SaveData.SetSaveString(saveString);

            GameControl.script.AutoSaveGame();

            //if (data.gameControlData != null) GameControl.script.data = data.gameControlData;
            //if (data.moneyControlData != null) MoneyControl.script.data = data.moneyControlData;
        }
        catch (Exception e)
        {
            //if (GameControl.DEBUG_MODE)
            //    PopupControl.script.ShowNotification("Error 2: " + e.Message);
            Debug.LogWarning("Error 2: " + e.Source + "/" + e.Data + "/" + e.InnerException + "/" + e.Message + "/" + e.StackTrace);

            return false;
        }

        return true;
    }

    public static string GetSaveInfo(string slot)
    {
        string res = "";

        try
        {
            Debug.Log("Try to get from : " + SaveFilePath(slot));
            SaveData data = PrepareSaveData(SaveFilePath(slot));

            if (data == null)
            {
                throw new System.Exception("data was not loaded");
            }

            //Debug.Log("Read saveID:" + data.gameData.saveID);

            //res += "Saving time: " + data.savingTime + Environment.NewLine;
            //res += "Language: " + data.gameData.TARGET_LANGUAGE;

            //TimeSpan timeTaken = DateTime.Now - data.savingTime;

            //Debug.Log("Loaded data. Last login date was: " + StatsControl.script.data.lastLoginDate);
        }
        catch (Exception e)
        {
            //if (GameControl.DEBUG_MODE)
            //    PopupControl.script.ShowNotification("Error 3: " + e.Message);
            Debug.LogWarning("Error 3: " + e.Source + "/" + e.Data + "/" + e.InnerException + "/" + e.Message + "/" + e.StackTrace);

            res = "Save slot empty";
        }

        return res;
    }

    private static SaveData PrepareSaveData(string path)
    {
        try
        {
            string allText = File.ReadAllText(path);
            string[] splitText = allText.Split(new[] { HASH_DELIMITER }, StringSplitOptions.None);
            if (!GameControl.DEBUG_MODE && splitText[0] != Utilities.CalculateMD5Hash(splitText[1]))
            {
                throw new Exception("Hash did not match");
            }
            return (SaveData)StringSerializationAPI.Deserialize(typeof(SaveData), Utilities.EncryptDecrypt(splitText[1]));
        }
        catch
        {
            return new SaveData();
        }
    }

    private static SaveData VerifySaveData(string loadString)
    {
        try
        {
            string allText;
            allText = Utilities.Decrypt(File.ReadAllText(loadString));
            //else allText = Utilities.Decrypt(loadString);
            Debug.Log("Decrypted: " + allText);
            string[] splitText = allText.Split(new[] { HASH_DELIMITER }, StringSplitOptions.None);
            if (!GameControl.DEBUG_MODE && splitText[0] != Utilities.CalculateMD5Hash(splitText[1]))
            {
                throw new Exception("Hash did not match");
            }
            return (SaveData)StringSerializationAPI.Deserialize(typeof(SaveData), splitText[1]);
        }
        catch
        {
            return new SaveData();
        }
    }

    //private static void ResetData()
    //{
    //    GameControl.script.data = new GameControlData();
    //    MoneyControl.script.data = new MoneyControlData();
    //    ManagerControl.script.ResetManagers();
    //    UpgradeControl.script.DespawnUpgrades();
    //    UpgradeControl.script.data = new UpgradeControlData();

    //}

    public static string SaveFilePath(string slotName = "")
    {
        return Application.persistentDataPath + Path.DirectorySeparatorChar + "save.json";
    }

    public static void DEBUG_MakeReadableSavefile()
    {
        if (!GameControl.DEBUG_MODE) return;
        //Load();
        Save(false);
    }
}

// === This is required to guarantee a fixed serialization assembly name, which Unity likes to randomize on each compile
// Do not change this
public sealed class VersionDeserializationBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
        {
            Type typeToDeserialize = null;

            assemblyName = Assembly.GetExecutingAssembly().FullName;

            // The following line of code returns the type. 
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return typeToDeserialize;
        }

        return null;
    }
}