﻿using UnityEngine;    // For Debug.Log, etc.

using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections.Generic;

// === This is the info container class ===
[Serializable()]
public class SaveDataOLD : ISerializable
{
    public string version;

    // === Values ===
    // Edit these during gameplay
    public DateTime savingTime;
    public GameData gameData;
    public ReviewData reviewData;
    public WordData wordData;

    // === /Values ===

    // The default constructor. Included for when we call it during Save() and Load()
    public SaveDataOLD() { version = Application.version; }
    public SaveDataOLD(string _version) { version = _version; }

    // This constructor is called automatically by the parent class, ISerializable
    // We get to custom-implement the serialization process here
    public SaveDataOLD(SerializationInfo info, StreamingContext ctxt)
    {
        // Get the values from info and assign them to the appropriate properties. Make sure to cast each variable.
        // Do this for each var defined in the Values section above

        //saveGameVersion = (string)info.GetValue("saveGameVersion", typeof(string));
        savingTime = (DateTime)info.GetValue("savingTime", typeof(DateTime));
        gameData = (GameData)info.GetValue("gameData", typeof(GameData));
        reviewData = (ReviewData)info.GetValue("reviewData", typeof(ReviewData));
        wordData = (WordData)info.GetValue("wordData", typeof(WordData));
    }

    // OLD
    // Required by the ISerializable class to be properly serialized. This is called automatically
    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        // Repeat this for each var defined in the Values section
        gameData = GameControl.script.data;

        PrepareInfo(info);
    }

    public void GetObjectData()
    {
        // Repeat this for each var defined in the Values section
        savingTime = DateTime.Now;
        
        gameData = GameControl.script.data;
        reviewData = ReviewControl.script.data;
        wordData = WordControl.script.data;
    }

    public void PrepareInfo(SerializationInfo info)
    {
        info.AddValue("savingTime", DateTime.Now);
        info.AddValue("gameData", gameData);
        info.AddValue("reviewData", reviewData);
        info.AddValue("wordData", wordData);
    }
}

// === This is the class that will be accessed from scripts ===
public class SaveLoadOLD
{
    static string HASH_DELIMITER = "###";
    public static string hash;

    // Call this to write data
    public static void Save(bool encrypt = true)  // Overloaded
    {
        if (!GameControl.script.gameLoaded) return; //if the prefiouse game is not loaded, don't overwrite it

        if (encrypt) DEBUG_MakeReadableSavefile();

        //Debug.Log("Save file to: " + SaveFilePath(GameControl.script.activeSaveSlot));
        Save(SaveFilePath(GameControl.script.activeSaveSlot), encrypt);
    }
    public static void Save(string filePath, bool encrypt)
    {
        SaveData data = new SaveData();
        data.GetObjectData();
        GameControl.script.data.saveID++;

        string serString;
        if (encrypt) serString = Utilities.EncryptDecrypt(StringSerializationAPI.Serialize(typeof(SaveData), data));
        else serString = StringSerializationAPI.Serialize(typeof(SaveData), data);
        hash = Utilities.CalculateMD5Hash(serString);
        serString = hash + HASH_DELIMITER + serString;
        if (encrypt) File.WriteAllText(filePath, serString);
        else File.WriteAllText(SaveFilePath("save-readable" + GameControl.script.activeSaveSlot), serString);
    }

    // Noskaidro tālāko save versiju, un izsauc tā ielādi
    public static bool Load()
    {
        //Debug.Log("----- LOADING");

        return Load(SaveFilePath(GameControl.script.activeSaveSlot));
    }  // Overloaded
    public static bool Load(string filePath)
    {
        Debug.Log("LOAD " + filePath);
        try
        {
            SaveData data = PrepareSaveData(filePath);

            if (data == null)
            {
                Debug.LogWarning("data or gameData is null!!");
                throw new System.Exception("gameData was not loaded");
            }

            //Debug.Log("Read saveID:" + data.gameData.saveID + Environment.NewLine + "Version: " + data.version);

            //TimeSpan timeTaken = DateTime.Now - data.savingTime;

            //if (data.gameData != null) GameControl.script.data = data.gameData;
            //if (data.reviewData != null) ReviewControl.script.data = data.reviewData;
            //if (data.wordData != null) WordControl.script.data = data.wordData;

            //Debug.Log("Loaded data. Last login date was: " + StatsControl.script.data.lastLoginDate);
        }
        catch (Exception e)
        {
            if (GameControl.DEBUG_MODE)
                PopupControl.script.ShowNotification("Error 2: " + e.Message);
            Debug.LogWarning("Error 2: " + e.Source + "/" + e.Data + "/" + e.InnerException + "/" + e.Message + "/" + e.StackTrace);

            return false;
        }
        finally
        {
            GameControl.script.gameLoaded = true;
        }

        return true;
    }

    //public static string GetSaveInfo(string slot)
    //{
    //    string res = "";
        
    //    try
    //    {
    //        Debug.Log("Try to get from : " + SaveFilePath(slot));
    //        SaveData data = PrepareSaveData(SaveFilePath(slot));

    //        if (data == null)
    //        {
    //            throw new System.Exception("data was not loaded");
    //        }
    //        if (data.gameData == null)
    //        {
    //            throw new System.Exception("gameData was not loaded");
    //        }

    //        Debug.Log("Read saveID:" + data.gameData.saveID);

    //        res += "Saving time: " + data.savingTime + Environment.NewLine;
    //        res += "Language: " + data.gameData.TARGET_LANGUAGE;

    //        TimeSpan timeTaken = DateTime.Now - data.savingTime;

    //        //Debug.Log("Loaded data. Last login date was: " + StatsControl.script.data.lastLoginDate);
    //    }
    //    catch (Exception e)
    //    {
    //        //if (GameControl.DEBUG_MODE)
    //        //    PopupControl.script.ShowNotification("Error 3: " + e.Message);
    //        Debug.LogWarning("Error 3: " + e.Source + "/" + e.Data + "/" + e.InnerException + "/" + e.Message + "/" + e.StackTrace);

    //        res = "Save slot empty";
    //    }

    //    return res;
    //}

    private static SaveData PrepareSaveData(string path)
    {
        try
        {
            string allText = File.ReadAllText(path);
            string[] splitText = allText.Split(new[] { HASH_DELIMITER }, StringSplitOptions.None);
            if (!GameControl.DEBUG_MODE && splitText[0] != Utilities.CalculateMD5Hash(splitText[1]))
            {
                throw new Exception("Hash did not match");
            }
            return (SaveData)StringSerializationAPI.Deserialize(typeof(SaveData), Utilities.EncryptDecrypt(splitText[1]));
        }
        catch
        {
            return new SaveData();
        }
    }
    
    public static string SaveFilePath(string slotName = "")
    {
        return Application.persistentDataPath + Path.DirectorySeparatorChar + "save-" + slotName + ".json";
    }

    public static void DEBUG_MakeReadableSavefile()
    {
        if (!GameControl.DEBUG_MODE) return;
        //Load();
        Save(false);
    }
}

// === This is required to guarantee a fixed serialization assembly name, which Unity likes to randomize on each compile
// Do not change this
//public sealed class VersionDeserializationBinder : SerializationBinder
//{
//    public override Type BindToType(string assemblyName, string typeName)
//    {
//        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
//        {
//            Type typeToDeserialize = null;

//            assemblyName = Assembly.GetExecutingAssembly().FullName;

//            // The following line of code returns the type. 
//            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

//            return typeToDeserialize;
//        }

//        return null;
//    }
//}