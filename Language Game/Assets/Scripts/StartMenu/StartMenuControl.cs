﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class StartMenuControl : MonoBehaviour {

    public static StartMenuControl script;

    public TMP_Dropdown languageDropdown;
    public List<TextMeshProUGUI> saveSlotTexts;

    public GameObject mainPanel;
    public GameObject saveSlotsPanel;

    private void Awake()
    {
        script = this;
    }

    // Use this for initialization
    void Start () {
        Debug.Log("Start");
        PrepareSlotInfos();

        PlayerPrefs.SetString("activeLanguage", "ru");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PrepareSlotInfos()
    {
        for (int i = 0; i < saveSlotTexts.Count; i++)
        {
            saveSlotTexts[i].text = SaveLoad.GetSaveInfo((i+1).ToString());
        }
    }

    public void ShowMainPanel()
    {
        mainPanel.SetActive(true);
        saveSlotsPanel.SetActive(false);
    }
    public void ShowSaveSlotsPanel()
    {
        mainPanel.SetActive(false);
        saveSlotsPanel.SetActive(true);
    }

    public void SelectLanguage(int i)
    {
        Debug.Log("Language selected: " + languageDropdown.value);
        string languageString;
        switch (languageDropdown.value)
        {
            case 0:
                languageString = "ru";
                break;
            case 1:
                languageString = "lv";
                break;
            default:
                languageString = "ru";
                break;
        }

        PlayerPrefs.SetString("activeLanguage", languageString);
    }

    public void SetSaveSlot(string slot)
    {
        PlayerPrefs.SetString("activeSaveSlot", slot);
        LoadLevel();
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("TestScene", LoadSceneMode.Single);
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }
}
