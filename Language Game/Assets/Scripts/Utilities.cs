﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

public static class Utilities
{
    public static System.Random rnd = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rnd.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static Dictionary<string, string> GetDictionary(string fileName)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            TextAsset file = (TextAsset)Resources.Load(fileName);
            string[] lines = file.text.Split("\n"[0]);

            for (int i = 0; i < lines.Length; i++)
            {
                //Debug.Log("Line read: " + lines[i]);
                if (lines[i].Trim() == "") continue;
                string[] entries = lines[i].Split(';');
                if (entries.Length > 0)
                    dict.Add(entries[0].Trim(), entries[1].Trim());
            }

        }
        catch (Exception e)
        {
            Debug.LogError("Error reading dictionary - " + e.Message);
        }

        return dict;
    }

    public static Dictionary<string, string> GetOLWords(string fileName)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            TextAsset file = (TextAsset)Resources.Load(fileName);
            string[] lines = file.text.Split("\n"[0]);

            for (int i = 0; i < lines.Length; i++)
            {
                //Debug.Log("Line read: " + lines[i]);
                if (lines[i].Trim() == "") continue;
                string[] entries = lines[i].Split(';');
                if (entries.Length > 0)
                {
                    dict.Add(entries[0].Trim(), entries[1].Trim());
                    //Debug.Log("Add to OL: " + entries[0].Trim() + " " + entries[1].Trim());
                }
            }

        }
        catch (Exception e)
        {
            Debug.LogError("Error reading OL words - " + e.Message);
        }

        return dict;
    }

    public static List<string[]> GetWordChildren()
    {
        List<string[]> result = new List<string[]>();

        try
        {
            TextAsset file = (TextAsset)Resources.Load("word-relationships");
            string[] lines = file.text.Split("\n"[0]);

            for (int i = 0; i < lines.Length; i++)
            {
                //Debug.Log("Line read: " + lines[i]);
                if (lines[i].Trim() == "") continue;
                string[] entries = lines[i].Split(';');
                if (entries.Length > 0)
                {
                    result.Add(new string[] { entries[0].Trim(), entries[1].Trim(), entries[2].Trim() });
                }
            }

        }
        catch (Exception e)
        {
            Debug.LogError("Error reading children - " + e.Message);
        }

        return result;
    }

    //https://stackoverflow.com/questions/48087/select-n-random-elements-from-a-listt-in-c-sharp
    public static List<WordInfo> GetRandomList(List<WordInfo> list, int number)
    {
        if (number > list.Count) return null;
        
        List<WordInfo> res = new List<WordInfo>();
        int selectedCount = 0;
        for (int i = 0; i < list.Count; i++)
        {
            double chance = (double)(number - selectedCount) / (list.Count - i);
            if (rnd.NextDouble() < chance)
            {
                res.Add(list[i]);
                selectedCount++;

                if (selectedCount == number) break;
            }
        }
        res.Shuffle();

        return res;
    }

    public static string CalculateMD5Hash(string input)
    {
        MD5 md5 = System.Security.Cryptography.MD5.Create();

        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes("vocabPETERIS" + input);
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }

    public static string EncryptDecrypt(string textToEncrypt)
    {
        StringBuilder inSb = new StringBuilder(textToEncrypt);
        StringBuilder outSb = new StringBuilder(textToEncrypt.Length);
        char c;
        for (int i = 0; i < textToEncrypt.Length; i++)
        {
            c = inSb[i];
            c = (char)(c ^ 129);
            outSb.Append(c);
        }
        return outSb.ToString();
    }

    public static string Encrypt(string s)
    {
        if (GameControl.DEBUG_MODE) return s;
        string str = Convert.ToBase64String(Encoding.UTF8.GetBytes(s));
        return str;
        //return str.Substring(0, str.Length - 2);
    }
    public static string Decrypt(string s)
    {
        //Debug.Log("Decrypt " + s);
        if (GameControl.DEBUG_MODE) return s;
        return Encoding.UTF8.GetString(Convert.FromBase64String(s));
    }

    public static int RandomIntFromRange(int minVal, int maxVal, int seed = -1)
    {
        if (seed > -1)
        {
            System.Random r = new System.Random(seed);
            return (int)Math.Round(r.NextDouble() * (maxVal - minVal) + minVal);
        }
        return (int)Math.Round(rnd.NextDouble() * (maxVal - minVal) + minVal);
    }

    public static double RandomDoubleFromRange(double minVal, double maxVal, int seed = -1)
    {
        if (seed > -1)
        {
            System.Random r = new System.Random(seed);
            return r.NextDouble() * (maxVal - minVal) + minVal;
        }
        return rnd.NextDouble() * (maxVal - minVal) + minVal;
    }
}
