﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WordElement : MonoBehaviour
{
    public TextMeshProUGUI TLWordText;
    public TextMeshProUGUI OLWordText;

    public GameObject lockedPanel;
    public Button unlockButton;
    
    private WordInfo activeWord;

    // Use this for initialization
    void Start () {
        //InvokeRepeating("UpdateInvoke", 0.21f, 0.21f);
	}
	
	// Update is called once per frame
	void UpdateInvoke () {
	    
	}
	
	public void UpdateLock() {
		if (activeWord.saveInfo.isUnlocked) {
		    lockedPanel.SetActive(false);
		}
		else {
		    lockedPanel.SetActive(true);
		}
	}

    public void ShowElement(WordInfo word)
    {
        activeWord = word;
        TLWordText.text = word.wordInTL;
        OLWordText.text = word.wordInOL;
        UpdateLock();
    }
    
    public void UnlockWord()
    {
        activeWord.saveInfo.isUnlocked = true;
        activeWord.PlaySound();
        activeWord.FindWord();
        
        UpdateLock();
    }

    public void PlaySound()
    {
        activeWord.PlaySound();
    }
}
