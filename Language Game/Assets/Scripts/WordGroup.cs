﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordGroup : MonoBehaviour
{
    public List<ClickableObject> groupElements;
    public ClickableObject mainElement;
    public bool onlyTogether = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void HighlightGroup()
    {
        for (int i = 0; i < groupElements.Count; i++)
        {
            groupElements[i].HighlightObject();
        }
    }

    public void CancelHighlightGroup()
    {
        for (int i = 0; i < groupElements.Count; i++)
        {
            groupElements[i].CancelHighlightObject();
        }
    }

    public void SelectGroup()
    {
        mainElement.objectInfo.Select();
    }
}
