﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WordInfo
{
    //public string wordInEnglish; //saglabāšanai
    public WordSaveInfo saveInfo;

    public string wordInOL; //original language
    public string wordInTL; //target language

    public WordInfo childVerb;
    public WordInfo childAdjective;

    public AudioClip wordSound;

    public void SetTL()
    {
        try
        {
            wordInTL = WordControl.script.dictionary[wordInOL];
            //Debug.Log("Sounds/" + GameControl.script.TARGET_LANGUAGE + "/" + wordInOL);
            wordSound = Resources.Load("Sounds" + Path.DirectorySeparatorChar + GameControl.script.data.TARGET_LANGUAGE + Path.DirectorySeparatorChar + wordInOL) as AudioClip;
        }
        catch
        {
            Debug.LogWarning("Word '" + wordInOL + "' sound or dictionary not found at " + "Sounds/" + GameControl.script.data.TARGET_LANGUAGE + "/" + wordInOL);
        }
    }

    public void FindWord()
    {
        if (WordControl.script.data.foundWords.Contains(this)) return;

        saveInfo.isUnlocked = true;
        WordControl.script.data.foundWords.Add(this);
        ReviewControl.script.AddWordToBoxes(this);
    }

    public override bool Equals(object Obj)
    {
        if (object.ReferenceEquals(Obj, null)) return false;

        WordInfo other = (WordInfo)Obj; 
        return (this.wordInOL == other.wordInOL);
    }
    public static bool operator ==(WordInfo person1, WordInfo person2)
    {
        if (object.ReferenceEquals(person1, null))
        {
            return object.ReferenceEquals(person2, null);
        }
        return person1.Equals(person2);
    }
    public static bool operator !=(WordInfo person1, WordInfo person2)
    {
        return !(person1 == person2);
    }
    public override int GetHashCode()
    {
        return wordInOL.GetHashCode();
    }

    public void PlaySound()
    {
        Debug.Log("Play sound");
        GameControl.script.wordSource.clip = wordSound;
        GameControl.script.wordSource.Play();
    }
}
