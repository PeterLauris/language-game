﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WordLine : MonoBehaviour
{
    public TextMeshProUGUI targetWordText;
    public TextMeshProUGUI sourceWordText;
    public Button button;
    public WordInfo wordInfo;
    

    public void Set (WordInfo wi)
    {
        wordInfo = wi;
        targetWordText.text = wi.wordInTL;
        sourceWordText.text = wi.wordInOL;

        if (wi.saveInfo.isFound) button.interactable = true;
        else button.interactable = false;
    }

    public void WordLineClicked()
    {
        wordInfo.PlaySound();
    }
}
