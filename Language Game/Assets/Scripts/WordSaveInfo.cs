﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordSaveInfo
{
    public string wordInEnglish;
    public bool isUnlocked = false;
    public bool isFound = false;
    public DateTime foundTime;
    public ReviewInfo reviewInfo;
    public int synonymIdx;

    public WordSaveInfo(string _word)
    {
        wordInEnglish = _word;
        foundTime = DateTime.MinValue;
        reviewInfo = new ReviewInfo();
        synonymIdx = 0;
    }
}
