﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordSelectedPanel : MonoBehaviour
{
    public static WordSelectedPanel script;

    public GameObject panel;
    public WordElement elementNoun;
    public WordElement elementVerb;
    public WordElement elementAdjective;
    
    void Awake ()
    {
        script = this;
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panel.activeSelf)
                HidePanel();
        }
    }

    public void ShowPanel(WordInfo word)
    {
        elementNoun.ShowElement(word);
        if (word.childVerb != null)
        {
            elementVerb.ShowElement(word.childVerb);
            elementVerb.gameObject.SetActive(true);
        }
        else
        {
            elementVerb.gameObject.SetActive(false);
        }

        if (word.childAdjective != null)
        {
            elementAdjective.ShowElement(word.childAdjective);
            elementAdjective.gameObject.SetActive(true);
        }
        else
        {
            elementAdjective.gameObject.SetActive(false);
        }
        panel.SetActive(true);
    }

    public void HidePanel()
    {
        panel.SetActive(false);
    }
}
